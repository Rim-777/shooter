require 'bundler'
require 'bundler/setup'
require './app/models/url'
require './app/api'


Bundler.require(:default, File.expand_path('../Gemfile', __dir__))
