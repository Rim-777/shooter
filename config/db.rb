require 'active_record'
DB_URL = './database.yml'

ActiveRecord::Base.establish_connection(
    database: DB_URL
)