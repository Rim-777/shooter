require 'grape'
require 'active_record'

module Shooter
  class API < Grape::API
    default_format :json
    version 'v1', using: :header, vendor: 'shooter'

    resource :create do
      post :url do
        params[:shortсode] ? handle_existed_shortcode : handle_new_shortcode
      end
    end

    private

    def handle_existed_shortcode
      @url = Url.find_by_shortсode(params[:shortсode])
      # render error 409 if @url
      @url = Url.new(code: params[:url])
      if @url.save
        {shortcode: @url.shortсode}
      else
        {errors: @url.full_messages.join(', ')}
      end
    end

    def handle_new_shortcode
      shortcode = Url.generate(6)
      @url = Url.new(shortcodecode: shortcode, code: params[:url])
      if @url.save
        {shortcode: @url.shortсode}
      else
        {errors: @url.full_messages.join(', ')}
      end
    end
  end
end
