require 'active_record'
class Url < ActiveRecord::Base
  FORMAT = '^[0-9a-zA-Z_]{4,}$'
  validates :code, presence: true

  CHARSET = ['A'..'Z', 'a'..'z', 0..9].map(&:to_a).flatten.freeze

  def self.generate(len)
     CHARSET.sample(len).join
  end


  private
  def shortcode_correctness
    message = 'A short code must contain only letters and numbers and must be exactly 6 characters'
    errors.add(:invalid_shortcode, message) unless  shortcode =~ FORMAT
  end
end